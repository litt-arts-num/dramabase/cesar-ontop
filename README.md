# CESAR2RDF

**Mapping et conversion des données de CESAR en triplets RDF avec Ontop**

Contenu :

 * `ontop-cli-3.0.1` contient l'utilitaire Ontop en ligne de commande prêt à être employé  
 * `mapping_frbroo/` : contient les deux fichiers de configuration nécessaires à Ontop :
 * `db_connection.properties` : les paramètres de connexion à la base
 * **10 mappings** nécessaires pour convertir la base de SQL vers RDF :
  * companies.obda
  * copies-libraries.obda
  * notes.obda
  * performances.obda
  * persons.obda
  * publications-expressions_and_manifestations.obda
  * publishers.obda
  * scripts-manuscripts.obda
  * titles.obda
  * locations.obda


[**La documentation détaillée des mappings, commandes et paramètres d'Ontop  se trouve sur le wiki**](https://gitlab.com/litt-arts-num/dramabase/cesar2rdf/-/wikis/Home)

## Installer Ontop et le mapping complet

1. cloner localement le dépôt avec la commande


      git clone git@gitlab.com:litt-arts-num/dramabase/cesar2rdf.git
      cd cesar2rdf

2. `cd [$Path local cloned repositiory]/mapping_frbroo/`
3. Modifier `db_connection.properties` pour configurer la connexion à une des bases de données importées :


      jdbc.url= jdbc:mysql:[url_de_la_base_importée]?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC&sessionVariables=sql_mode='ANSI'
      jdbc.driver= com.mysql.cj.jdbc.Driver
      jdbc.user= [id_de_connexion]
      jdbc.password= [mot_de_passe_de_connexion]
      jdbc.name= c3524381-1588-47cc-9296-c6846ef73a82
      # par exemple : jdbc:mysql://localhost:3306/cesar? ...

## Utiliser Ontop CLI

Fonctionne avec Java 1.8

### Afficher l'aide


      cd ontop-cli-3.0.1/
      ./ontop

### Lancer une requête de test avec Ontop

Pour tester le bon fonctionnement d'un des 10 mappings.


      ./ontop query \
      -p ../mapping_frbroo/cesar_db.properties \
      -m ../mapping_frbroo/[MAPPING_FILE].obda \
      -t [ONTOLOGY_PATH].owl \
      -q ../queries-test/q0.txt  \
      --enable-annotations


La query du fichier `q0.txt` est une requête générique prévue à cet effet : elle sélectionne la liste des propriétés effectivement produites par le mapping, (en se limitant aux 40 premières cependant).

Il est possible de rédiger ses propres requêtes de test en SPARQL dans un fichier `.txt`, sur le modèle de celles qui sont fournies.

### Déployer un endpoint virtuel

Pour effectuer des requêtes SPARQL librement sur le résultat d'un mapping comme s'il était déjà dans un triplestore.


     ./ontop endpoint -p ../mapping_frbroo/cesar_db.properties -m ../mapping_frbroo/[MAPPING_FILE].obda -t [ONTOLOGY_PATH].owl --cors-allowed-origins http://yasgui.org

L'endpoint est ensuite accessible, via un navigateur web, à l'adresse `http://localhost:8080/`

### Matérialiser les triplets

La commande `materialize` permet de sauvegarder les triplets générés par mapping en différents formats, selon l'extension attribuée au fichier créé par le paramètre `-o` )`.ttl`; `xml/rdf` ou `nt`. Ces trois formats permettent un import dans un triplestore.


     ./ontop materialize -f turtle -m ../mapping_frbroo/[MAPPING_FILE].obda -p ../mapping_frbroo/cesar_db.properties -o [MATERIALIZED_RESULT_PATH].ttl --enable-annotations

En cas de doute, `3-2-1_Materilize.md` contient les commandes de test préconfigurées pour l'intégralité des mappings.  
